package br.ufrj.treinamentobackend.repository;

import br.ufrj.treinamentobackend.model.entity.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AlunoRepository extends JpaRepository<Aluno, Long> {
    Optional<Aluno> findAlunoByMatricula(String matricula);
}
