package br.ufrj.treinamentobackend.model.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class SituacaoMatricula {

    public final static String ATIVO = "A01";
    public final static String CANCELADO = "C01";
    public final static String TRANCADO = "T01";
    public final static String FORMADO = "F01";



    @Id
    private Long id;
    private String codigo;
    private String nome;

    public SituacaoMatricula() {
    }

    public SituacaoMatricula(Long id, String codigo, String nome) {
        this.id = id;
        this.codigo = codigo;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
