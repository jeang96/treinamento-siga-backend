package br.ufrj.treinamentobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SigaBackendApplication {

    public static void main(String[] args) {

        SpringApplication.run(SigaBackendApplication.class, args);
        System.out.println("server is running...");

    }

}
