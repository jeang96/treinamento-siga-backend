package br.ufrj.treinamentobackend.controller;

import br.ufrj.treinamentobackend.model.entity.Aluno;
import br.ufrj.treinamentobackend.repository.AlunoRepository;
import br.ufrj.treinamentobackend.service.AlunoService;
import br.ufrj.treinamentobackend.service.SituacaoMatriculaService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/aluno")
public class AlunoController {

    //    @Autowired não é recomendado
//    private AlunoRepository alunoRepository;
    private final AlunoRepository alunoRepository;
    private final AlunoService alunoService;
    private final SituacaoMatriculaService situacaoMatriculaService;

    public AlunoController(AlunoRepository alunoRepository, AlunoService alunoService, SituacaoMatriculaService situacaoMatriculaService) {
        this.alunoRepository = alunoRepository;
        this.alunoService = alunoService;
        this.situacaoMatriculaService = situacaoMatriculaService;
    }

    @GetMapping
    public List<Aluno> listAll() {
        return alunoRepository.findAll();
    }

    @GetMapping("/matricula/{matricula}")
    public Aluno findByMatricula(@PathVariable String matricula) {
        return alunoService.getAlunoByMatricula(matricula);
    }

    @PostMapping
    public Aluno createAluno(@RequestBody Aluno aluno) {
        return alunoService.criar(aluno);
    }

    @DeleteMapping("/{matricula}")
    public String deleteAluno(@PathVariable String matricula) {

        alunoService.deletar(matricula);
        return "Aluno deletado com sucesso.";
    }

    @PutMapping
    public Aluno editAluno(@RequestBody Aluno aluno) {
        return alunoService.editar(aluno);
    }

    @PutMapping(path = "/cancelar/{matricula}")
    public String cancelarMatriculaAluno(@PathVariable String matricula) {
        return situacaoMatriculaService.cancelarMatricula(matricula);
    }

}
