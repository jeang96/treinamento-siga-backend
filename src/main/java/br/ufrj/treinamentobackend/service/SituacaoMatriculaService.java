package br.ufrj.treinamentobackend.service;

import br.ufrj.treinamentobackend.model.entity.Aluno;
import br.ufrj.treinamentobackend.model.entity.SituacaoMatricula;
import br.ufrj.treinamentobackend.repository.AlunoRepository;
import br.ufrj.treinamentobackend.repository.SituacaoMatriculaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class SituacaoMatriculaService {
    private final SituacaoMatriculaRepository situacaoMatriculaRepository;
    private final AlunoRepository alunoRepository;

    public SituacaoMatriculaService(SituacaoMatriculaRepository situacaoMatriculaRepository, AlunoRepository alunoRepository) {
        this.situacaoMatriculaRepository = situacaoMatriculaRepository;
        this.alunoRepository = alunoRepository;
    }

    public String cancelarMatricula(String matricula) {
        Optional<Aluno> aluno = alunoRepository.findAlunoByMatricula(matricula);
        if (aluno.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aluno não encontrado!");
        }

        if ((aluno.get().getSituacaoMatricula().getCodigo().equals(SituacaoMatricula.CANCELADO) ||
                aluno.get().getSituacaoMatricula().getCodigo().equals(SituacaoMatricula.FORMADO))) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Impossível cancelar uma matrícula CANCELADO ou FORMADO!");
        }

        SituacaoMatricula situacaoCancelada = situacaoMatriculaRepository.findSituacaoMatriculaByCodigo(SituacaoMatricula.CANCELADO)
                        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Situação Matrícula não encontrada!"));

        aluno.get().setSituacaoMatricula(situacaoCancelada);
        alunoRepository.save(aluno.get());

       return "Matrícula cancelada com sucesso!";

    }
}


