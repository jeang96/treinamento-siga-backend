package br.ufrj.treinamentobackend.service;

import br.ufrj.treinamentobackend.model.entity.Aluno;
import br.ufrj.treinamentobackend.model.entity.SituacaoMatricula;
import br.ufrj.treinamentobackend.repository.AlunoRepository;
import br.ufrj.treinamentobackend.repository.SituacaoMatriculaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Struct;
import java.util.List;
import java.util.Optional;

@Service
public class AlunoService {

    private final AlunoRepository alunoRepository;
    private final SituacaoMatriculaRepository situacaoMatriculaRepository;

    public AlunoService(AlunoRepository alunoRepository, SituacaoMatriculaRepository situacaoMatriculaRepository) {
        this.alunoRepository = alunoRepository;
        this.situacaoMatriculaRepository = situacaoMatriculaRepository;
    }

    public Aluno getAlunoByMatricula(String matricula) {
        Optional<Aluno> optAluno = alunoRepository.findAlunoByMatricula(matricula);

        if (optAluno.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aluno não encontrado!");
        }

        return optAluno.get();
    }


    public Aluno criar(Aluno aluno) {
        if (alunoRepository.findAlunoByMatricula(aluno.getMatricula()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Aluno já existe");
        }

        SituacaoMatricula situacaoMatricula = situacaoMatriculaRepository.findSituacaoMatriculaByCodigo(SituacaoMatricula.ATIVO).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Situação matrícula não encontrada"));

        Aluno novoAluno = new Aluno();
        novoAluno.setNome(aluno.getNome());
        novoAluno.setMatricula(aluno.getMatricula());
        novoAluno.setHobbies(aluno.getHobbies());
        novoAluno.setSituacaoMatricula(situacaoMatricula);

        return alunoRepository.save(novoAluno);
    }

    @Transactional
    public void deletar(String matricula) {
        Aluno aluno = alunoRepository.findAlunoByMatricula(matricula).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Aluno não encontrado."));
        alunoRepository.delete(aluno);
    }


    public Aluno editar(Aluno novosDadosAluno) {

        Optional<Aluno> aluno = alunoRepository.findById(novosDadosAluno.getId());
        if (aluno.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aluno não encontrado");
        }

        System.out.println(aluno.get().getId());
        System.out.println(aluno.get().getMatricula());
        System.out.println("-------------------------");
        System.out.println(novosDadosAluno.getId());
        System.out.println(novosDadosAluno.getMatricula());

        Optional<Aluno> comparaAluno = alunoRepository.findAlunoByMatricula(novosDadosAluno.getMatricula());

        if(comparaAluno.isPresent() && comparaAluno.get().getId() != aluno.get().getId()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Matrícula já existente em outro usuário!");
        }


        aluno.get().setNome(novosDadosAluno.getNome());
        aluno.get().setMatricula(novosDadosAluno.getMatricula());
        aluno.get().setHobbies(novosDadosAluno.getHobbies());

        return alunoRepository.save(aluno.get());
    }
}
